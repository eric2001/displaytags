# DisplayTags

## Description
DisplayTags is a plugin for [Gallery 3](http://gallery.menalto.com/) which will display whatever tags the album/photo you're looking at has been tagged with.  It is intended as an interim solution until the Gallery 3 team can write up an official way to view the tags for a photo/video/album.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/89093).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

## History
**Version 1.4.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 28 August 2021.
>
> Download: [Version 1.4.0](/uploads/a81811f97a692ab6ed02fd9a2d12903d/displaytags140.zip)

**Version 1.3.1:**
> - Updated module.info file for changes in Gallery 3.0.1.
> - Released 03 August 2011.
>
> Download: [Version 1.3.1](/uploads/cfeaac620e602061ae4bd331adae594c/displaytags131.zip)

**Version 1.3.0:**
> - Updated for use with Gallery 3.0.1
> - Added warning messages to alert the user if the Tags module isn't active.
> - Released 26 January 2011.
>
> Download: [Version 1.3.0](/uploads/b085eac2d8a0806b668dce0019d8ec29/displaytags130.zip)

**Version 1.2.1:**
> - Updated to fix an issue with hyperlinks and current git.
> - Released 10 January 2010.
>
> Download: [Version 1.2.1](/uploads/6af77d3ebe3b1d52a599991fee92e6f1/displaytags121.zip)

**Version 1.2.0:**
> - Updated for recent changes to Gallery3's module API.
> - Released 30 December 2009.
>
> Download: [Version 1.2.0](/uploads/83c60096a63c8b1c0f04d7d5974245dd/displaytags120.zip)

**Version 1.1.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Updated for the new sidebar code in Gallery 3 Git
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 12 October 2009
>
> Download: [Version 1.1.0](/uploads/32d736415e43d287bda8d4bc06561472/displaytags110.zip)

**Version 1.0.1:**
> - Replace p::clean with html::clean for recent Gallery API change.
> - Released 31 August 2009
>
> Download: [Version 1.0.1](/uploads/d6d9b7b92690194f7d1cbfef8e68e778/displaytags101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 15 July 2009
>
> Download: [Version 1.0.0](/uploads/dc0b3b6e4eef73db79a1806e0ec31bd5/displaytags100.zip)
